package lesson05;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class TransposeTable {
    
    public static final int INPUT_TABLE_WIDTH = 6;
    public static final int INPUT_TABLE_HEIGHT = 4;
    
    public static final String FILE_NAME_INPUT = "/tmp/table-input.txt";
    public static final String FILE_NAME_OUTPUT = "/tmp/table-output.txt";

    public static void main(String[] args) {
        int[][] table1 = readTable(FILE_NAME_INPUT, INPUT_TABLE_WIDTH, INPUT_TABLE_HEIGHT);
        int[][] table2 = transposeTable(table1);
        writeTable(FILE_NAME_OUTPUT, table2);
    }
    

    public static int[][] readTable(String path, int width, int height) {
        int[][] table = new int[height][width];
        int rowIndex = 0;
        
        try {
            // Next variable could be optimised away. For educational purpose,
            // let's be explicit here.
            // Input-output operation, could fail. (FileNotFoundException)
            FileReader fr = new FileReader(path);

            BufferedReader br = new BufferedReader(fr);

            // Next three variables could be optimised away. For educational
            // purpose, let's be explicit here.
            String line;
            int[] tokens;

            // Input-output operation, could fail.
            while(br.ready()) {
                
                // Input-output operation, could fail.
                line = br.readLine();
                
                // Split line into parts separated by commas.
                tokens = splitStringToIntArray(line, ',');
                
                // Stash it away!
                table[rowIndex] = tokens;
                rowIndex++;
            }

            // Input-output operation, could fail.
            br.close();
            
        } catch (FileNotFoundException e) {
            System.err.println("Could not open input file! Aborting.");
            return null;
            
        } catch (IOException e) {
            System.err.println("Could not close input file! Aborting.");
            return null;
        }
        
        return table;
    }

    
    public static int[] splitStringToIntArray(String line, char delimiter) {
        int tokenCount = 1;
        
        // To initialise our output array we need to count the number of 
        // delimiters. The tokenCount is the number of delimiters + 1.
        for(int i = 0; i < line.length(); i++) {
            if(line.charAt(i) == delimiter) {
                tokenCount++;
            }
        }
        
        // Create output array.
        int[] array = new int[tokenCount];
        
        int currentTokenIndex = 0;
        int tokenStartIndex = 0;
        String token;
        
        // Iterate over the input string again. Everytime the delimiter is 
        // encountered, a substring is cut out and stored in the output array.
        // The substring starts after the last delimiter and ends at the
        // current position, where another delimiter was found.
        for(int i = 0; i < line.length(); i++) {
            if(line.charAt(i) == delimiter) {
                token = line.substring(tokenStartIndex, i);
                array[currentTokenIndex] = Integer.parseInt(token);
                tokenStartIndex = i + 1;
                currentTokenIndex++;
            }
        }
        
        // The very last token starts after the last delimiter and ends at
        // the end of the input string.
        token = line.substring(tokenStartIndex);
        array[currentTokenIndex] = Integer.parseInt(token);                
        return array;
    }
    
        
    public static int[][] transposeTable(int[][] table) {

        // Create the output array. Note the reversed order of lengths.
        int[][] transposedTable = new int[table[0].length][table.length];

        // Copy array.
        for(int y = 0; y < table.length; y++) {
            for(int x = 0; x < table[0].length; x++) {
                // Note the reversed pairs of indices left and right. 
                transposedTable[x][y] = table[y][x];
            }
        }
        
        return transposedTable;
    }
    

    public static void writeTable(String path, int[][] table) {

        try {
            // Input-output operation, could fail.
            FileWriter fw = new FileWriter(path);
            
            for(int y = 0; y < table.length; y++) {
                
                // We can rely on at least one column.
                fw.write(String.valueOf(table[y][0]));
                
                // Start index 1!
                for(int x = 1; x < table[y].length; x++) {
                    fw.write(',');
                    fw.write(String.valueOf(table[y][x]));
                }

                fw.write('\n');
            }

            // Input-output operation, could fail.
            fw.close();
            
        } catch (FileNotFoundException e) {
            System.err.println("Could not open input file! Aborting.");
            
        } catch (IOException e) {
            System.err.println("Could not close input file! Aborting.");
        }
        
    }

}
