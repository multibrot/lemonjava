package lesson06;

import resources.LemonWindow;

public class SpinnerAnimation {

    private static int GRID_WIDTH = 8;
    private static int GRID_HEIGHT = 6;

    private static LemonWindow window;
    
    
    public static void main(String[] args) {
        window = new LemonWindow("Spinner", GRID_WIDTH, GRID_HEIGHT);

        for(int y = 0; y < GRID_HEIGHT; y++) {
            drawHorizontalLine(y, false);
        }
        
        window.show();
            
        while(true) {
            slideRight();
            slideLeft();
            slideDown();
            slideUp();
            
            slideLeft();
            slideRight();
            slideUp();
            slideDown();
        }
    }
    
    
    public static void slideLeft() {
        for(int x = GRID_WIDTH - 1; x >= 0; x--) {                
            drawVerticalLine(x, true);
            window.draw();
            LemonWindow.sleep(100);
            drawVerticalLine(x, false);
        }
    }
    

    public static void slideRight() {
        for(int x = 0; x < GRID_WIDTH; x++) {                
            drawVerticalLine(x, true);
            window.draw();
            LemonWindow.sleep(100);
            drawVerticalLine(x, false);
        }
    }
    

    public static void slideUp() {
        for(int y = GRID_HEIGHT - 1; y >= 0; y--) {
            drawHorizontalLine(y, true);
            window.draw();
            LemonWindow.sleep(100);
            drawHorizontalLine(y, false);
        }
    }
    

    public static void slideDown() {
        for(int y = 0; y < GRID_HEIGHT; y++) {                
            drawHorizontalLine(y, true);
            window.draw();
            LemonWindow.sleep(100);
            drawHorizontalLine(y, false);
        }
    }
    

    public static void drawHorizontalLine(int y, boolean black) {
        drawLine(0, y, GRID_WIDTH - 1, y, black);
    }
    

    public static void drawVerticalLine(int x, boolean black) {
        drawLine(x, 0, x, GRID_HEIGHT - 1, black);
    }
    

    public static void drawLine(int x1, int y1, int x2, int y2, boolean black) {
        String imageID;
        int a;
        
        if(black) {
            imageID = "black circle";
        } else {
            imageID = "white circle";
        }
        
        if(Math.abs(x1 - x2) > Math.abs(y1 - y2)) {
            for(int x = x1; x <= x2; x++) {
                a = y1 + (y2 - y1) * (x - x1) / (x2 - x1);
                window.setImage(imageID, x, a);                    
            }
        } else {
            for(int y = y1; y <= y2; y++) {
                a = x1 + (x2 - x1) * (y - y1) / (y2 - y1);
                window.setImage(imageID, a, y);                    
            }
        }
    }
    
}
