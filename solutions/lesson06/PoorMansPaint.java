package lesson06;

import java.awt.event.KeyEvent;
import java.io.IOException;

import resources.LemonEvent;
import resources.LemonWindow;

public class PoorMansPaint {

    public static final String[] BRUSHES = {
            "white circle", "black circle", "slash", "backslash"
    };
    
    
    private static LemonWindow window;
    private static int brushIndex = 0;
    private static boolean stop = false;
    
    
    public static void main(String[] args) {
        
        window = new LemonWindow("Poor Man's Paint", 7, 7);
        LemonEvent[] events;
        
        window.show();
        
        while(!stop) {
            
            // Variant: for-each loop
            /*
            for(LemonEvent event : window.getInputEvents()) {
                handleEvent(event);
            }
            */
            
            events = window.getInputEvents();
            
            for(int i = 0; i < events.length; i++) {
                handleEvent(events[i]);
            }
            
            LemonWindow.sleep(10);
        }
        
        window.hide();
    }


    private static void handleEvent(LemonEvent event) {
        switch(event.eventType) {

        case LemonEvent.EVENT_TYPE_KEYBOARD_EVENT:
            handleKeyboardEvent(event);
            break;

        case LemonEvent.EVENT_TYPE_MOUSE_EVENT:
            handleMouseEvent(event);
            break;
        }
    }


    private static void handleMouseEvent(LemonEvent event) {
        switch(event.mouseButton) {
        
        case LemonEvent.MOUSE_BUTTON_LEFT:
            window.setImage(BRUSHES[brushIndex], event.mouseX, event.mouseY);
            break;
            
        case LemonEvent.MOUSE_BUTTON_RIGHT:
            window.setImage(null, event.mouseX, event.mouseY);
            break;
            
        case LemonEvent.MOUSE_WHEEL_UP:
            brushIndex = (brushIndex - 1 + BRUSHES.length) % BRUSHES.length;
            System.out.println("Current brush: " + BRUSHES[brushIndex]);
            break;
            
        case LemonEvent.MOUSE_WHEEL_DOWN:
            brushIndex = (brushIndex + 1) % BRUSHES.length;
            System.out.println("Current brush: " + BRUSHES[brushIndex]);
            break;
        }
    }
    
    
    private static void handleKeyboardEvent(LemonEvent event) {        
        switch(event.keyCode) {
        
        case KeyEvent.VK_ESCAPE:
            stop = true;
            break;
            
        case KeyEvent.VK_R:
            for(int y = 0; y < 7; y++) {
                for(int x = 0; x < 7; x++) {
                    window.setImage(null, x, y);
                }
            }
            break;
            
        case KeyEvent.VK_SPACE:
            try {
                LemonWindow.playSound("resources/tada.wav");
            } catch (IOException e) {
                System.out.println("Could not play sound. Shouganai.");
            }
            break;
        }
    }

}
