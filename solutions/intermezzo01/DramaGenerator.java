package intermezzo01;

import java.util.Random;


public class DramaGenerator {
    
    public static final String[] FORMATS_EXPOSITION = {
            "Once upon a time, %1$s and %2$s met %3$s.",
            "One dark, dark night %3$s, %1$s bumped into %2$s.",
            "\"What a nice day for a visit %3$s!\", %1$s thought, when suddenly %2$s appeared.",
    };
    public static final String[] FORMATS_RISE = {
            "Then %2$s stole %1$s's favourite plastic toy.",
            "%2$s told %1$s to grow a smaller nose.",
            "\"Are you talking to me?\", %2$s said."
    };
    public static final String[] FORMATS_CLIMAX = {
            "%1$s slapped %2$s with a fish.",
            "Suddenly %1$s emptied a bucket full of water over %2$s.",
            "Keeping the cool, %1$s pretended to call the cops.",
    };
    public static final String[] FORMATS_FALL = {
            "Finally %2$s started to beg for mercy.",
            "In front of %1$s suddenly %2$s fell on the knees and started crying.",
            "Then %2$s blushed and looked at the ground with a smile in the face.",
    };
    public static final String[] FORMATS_CATASTROPHE = {
            "And the moral is: People like %2$s should be careful around %1$s.",
            "Afterwards %1$s and %2$s became friends and lived happily ever after.",
            "\"Cool beans?\", %2$s asked; \"Cool beans\", %1$s replied.",
    };
    public static final String[] FORMATS_LOCATION_CHANGE = {
            "Later %1$s arrived %3$s, where %2$s already waited.",
            "%3$s, %1$s and %2$s met again.",
            "\"Here %3$s\", %1$s said, \"you won't escape me now\"."
    };

    public static final String[] CHARACTER_NAMES = {
            "Batman",
            "Satan",
            "a bad dream",
            "Marilyn Monroe",
            "Bill Gates",
            "Super Mario",
            "Napoleon",
            "Albert Einstein",
            "Donald Trump",
            "Yo Mama",
    };

    public static final String[] LOCATION_NAMES = {
            "in the castle", 
            "on the graveyard", 
            "in a supermarket",
            "between here and there",
            "in hell",
            "in the basement",
            "on the internet",
    };

    private static Random random = new Random();
    
    
    public static void main(String[] args) {
        
        String[] x = chooseRandomElements(new String[]{"a", "b", "c"}, 2);
        System.out.println(x[0]);
        System.out.println(x[1]);
        System.exit(1);
        
        String[] names = chooseRandomElements(CHARACTER_NAMES, 2);        
        String[] locations = chooseRandomElements(LOCATION_NAMES, 2);
        
        String exposition = chooseRandomElement(FORMATS_EXPOSITION);
        String rise = chooseRandomElement(FORMATS_RISE);
        String climax = chooseRandomElement(FORMATS_CLIMAX);
        String change_loc = chooseRandomElement(FORMATS_LOCATION_CHANGE);
        String fall = chooseRandomElement(FORMATS_FALL);
        String catastophe = chooseRandomElement(FORMATS_CATASTROPHE);
        
        System.out.println(String.format(exposition, names[0], names[1], locations[0]));
        System.out.println(String.format(rise,       names[0], names[1]));
        System.out.println(String.format(change_loc, names[0], names[1], locations[1]));
        System.out.println(String.format(climax,     names[0], names[1]));
        System.out.println(String.format(fall,       names[0], names[1]));
        System.out.println(String.format(catastophe, names[0], names[1]));
    }
    
    
    public static String chooseRandomElement(String[] array) {
        return array[random.nextInt(array.length)];
    }
    
    
    /*
     * Select `amount` random elements from `array` and put it into a new 
     * array of length `amount`.
     */
    public static String[] chooseRandomElements(String[] array, int amount) {
        String[] arrayCopy = new String[array.length];
        String[] selectedElements = new String[amount];
        int randomIndex;
        
        // Copy original array elements into working array.
        for(int i = 0; i < arrayCopy.length; i++) {
            arrayCopy[i] = array[i];
        }
        
        // From working array copy only the first required elements. Replace 
        // elements to avoid picking the same next time.  In each iteration, 
        // only choose from the first (array length - i) elements.
        for(int i = 0; i < selectedElements.length; i++) {
            randomIndex = random.nextInt(arrayCopy.length - i);
            selectedElements[i] = arrayCopy[randomIndex];
            arrayCopy[randomIndex] = arrayCopy[arrayCopy.length - 1 - i];
        }
        
        return selectedElements;
    }

}
