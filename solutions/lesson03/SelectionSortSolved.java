package lesson03;

import java.util.Arrays;
import java.util.Random;

public class SelectionSortSolved {
    
    public static void main(String[] args) {
        
        // Use fixed set of values ...
        int[] values = {84, 56, 90, 46, 50, 88, 18, 35, 28, 96};
        
        // ... or use randomly generated values instead.
        //int[] values = generateArray(10);
              
        // Show unsorted array.
        System.out.println(Arrays.toString(values));
        
        // Sort array.
        selectionSort(values);
        
        // Show sorted array.
        System.out.println(Arrays.toString(values));
    }
    
    
    /*
     * The Selection Sort algorithm goes like this: 
    
        for every element A in the array, except the last one
            set MINIMUM to A
            
            for every element B in the array, except the first one
                if B is smaller than MINIMUM
                    set MINIMUM to B
                    
            swap A with MINIMUM        

     * The way of description is called pseudo-code, a structured way of 
     * writing out an algoritm, including loops, conditionals, variables and 
     * so on, but without actually writing real code. This is great for 
     * brainstorming and development, or, well, communication.
     */
    public static void selectionSort(int[] values) {
        
        // Create a temporary container for swapping values. Store it outside
        // the loops, so it can be reused. #optimisation
        int temp;
        
        // A container that stores the index of the smallest element (for the
        // given iteration.
        int minIndex;
        
        // We need to compare each element with another one. Hence we are
        // iterating here over all left (length - 1) elements of the array, so
        // we can compare all of them with the right (length - 1) elments.
        for(int i = 0; i < values.length - 1; i++) {
        
            // Until we know better, we assume the smallest value is at (i).
            minIndex = i;
            
            // Now pick the element to be compare with from the right (length
            // - 1) elements.
            // #optimisation We can start not at (j = 1), but at (j = i + 1),
            // since we can expect that all first (i) elements have already
            // been sorted.
            for(int j = i + 1; j < values.length - 1; j++) {
                
                // Is there an element at (j), that is smaller than the
                // currently remembered element at (minIndex)?
                if(values[minIndex] > values[j]) {
                    minIndex = j;
                }
                
                // A smallest element was determined, which should be now
                // swapped in place with the current (i) element.
                // To have both elements swap places, we need a temporary
                // location to store a value.
                temp = values[i];
                values[i] = values[minIndex];
                values[minIndex] = temp;
            }
        }
    }
    
    
    public static int[] generateArray(int length) {
        Random random = new Random();
        int[] array = new int[length];
        for(int i = 0; i < array.length; i++) {
            // let all numbers have two digits
            array[i] = random.nextInt(89)+ 10; 
        }
        return array;
    }
}
