package lesson03;

import resources.LemonWindow;

public class FunnyGeometrySolved {

    private static int GRID_WIDTH = 20;
    private static int GRID_HEIGHT = 15;
    
    private static LemonWindow window;
    
    
    public static void main(String[] args) {
        window = new LemonWindow("Funny Geometry", GRID_WIDTH, GRID_HEIGHT);
        clearGrid();
        drawFilledRect(1, 10, 5, 12);
        drawRectNaive(0, 0, 4, 5);
        drawRect(2, 2, 14, 8);
        drawLine(9, 6, 13, 14);
        drawLine(11, 5, 19, 2);
        // Sophisticated, optional.
//      drawCircle(12, 6, 6);
        window.show();
    }
    
    
    public static void clearGrid() {
        for(int y = 0; y < GRID_HEIGHT; y++) {
            for(int x = 0; x < GRID_WIDTH; x++) {
                window.setImage("white circle", x, y);
            }
        }
    }
    

    public static void drawFilledRect(int x1, int y1, int x2, int y2) {
        int xMin;
        int xMax;
        int yMin;
        int yMax;

        // Find horizontal minimum and maximum.
        if(x1 < x2) {
            xMin = x1;
            xMax = x2;
        } else {
            xMin = x2;
            xMax = x1;
        }

        // Find vertical minimum and maximum.
        if(y1 < y2) {
            yMin = y1;
            yMax = y2;
        } else {
            yMin = y2;
            yMax = y1;
        }
        
        // Draw from minimum to maximum in both dimensions.
        for(int y = yMin; y <= yMax; y++) {
            for(int x = xMin; x <= xMax; x++) {
                window.setImage("black circle", x, y);
            }
        }
    }


    public static void drawRectNaive(int x1, int y1, int x2, int y2) {
        // Quicker way to finding minimum and maximum using Java library.
        int xMin = Math.min(x1, x2);
        int xMax = Math.max(x1, x2);
        int yMin = Math.min(y1, y2);
        int yMax = Math.max(y1, y2);

        for(int y = yMin; y <= yMax; y++) {
            for(int x = xMin; x <= xMax; x++) {
                
                // Only draw, if y or x is equal to the respective minimum
                // or maximum.
                if((y == yMin || y == yMax) || (x == xMin || x == xMax)) {
                    window.setImage("black circle", x, y);                    
                }
            }
        }
    }


    public static void drawLine(int x1, int y1, int x2, int y2) {
        // Quicker way to finding minimum and maximum using Java library.
        int xMin = Math.min(x1, x2);
        int xMax = Math.max(x1, x2);
        int yMin = Math.min(y1, y2);
        int yMax = Math.max(y1, y2);
        
        // Helper variable, to not have all that math in one nasty line.
        int a;
        
        // We take the greater difference as basis for the mathematical 
        // formula to calculate the other part of the coordinate. That is, if
        // x-distance > y-distance, use y=mx+t, otherwise use x=ny+s.
        if(xMax - xMin > yMax - yMin) {
            for(int x = xMin; x <= xMax; x++) {
                
                // Here is some intuition for the math part:
                //
                // [*] (x - xMin) / (xMax - xMin) is a ratio between 0 and 1,
                //     since xMin <= x <= xMax. If x==xMin the ratio is 0 and 
                //     if x==xMax the ratio is 1.
                // [*] y1 + (y2 - y1) * 1 == y1 + y2 - y1 == y2 and
                //     y1 + (y2 - y1) * 0 == y1
                // [*] Both parts combined into the formula below means: We
                //     get y1 for x==xMin and y2 for x==xMax.

                // Note the uses of y1/y2 and xMin/xMax instead of y1/y2 and
                // x1/x2 or yMin/yMax together with xMin/xMax. Since we need
                // to use xMin/xMax for x of the loop, we also need it for
                // the ratio of x-distance to max-x-distance. However, we 
                // need to not use yMin/yMax in the other case, otherwise we
                // would always draw lines from top left to bottom right.
                // Hence we need to not consider absolute differences (i.e.
                // yMax-xMin and then added to yMin), but signed differences 
                // (i.e. y2-y1 and then added to the (first) y1).
                a = (int) Math.round(y1 + 1.0 * (y2 - y1) * (x - xMin) / (xMax - xMin));

                // The line below would also work, but then we get some
                // small rounding errors. These rounding errors can be
                // avoided by using floating point variables (float/double)
                // instead of integer. The code then gets a bit more verbose
                // then. The "1.0 *" converts to double and the "(int)"
                // converts to integer, since Math.round() always outputs a
                // double (or float) value.
//                a = y1 + (y2 - y1) * (x - xMin) / (xMax - xMin);
                
                window.setImage("black circle", x, a);                    
            }
        } else {
            for(int y = yMin; y <= yMax; y++) {
                
                // Same logic as above, but for x and y swapped.
                a = (int) Math.round(x1 + 1.0 * (x2 - x1) * (y - yMin) / (yMax - yMin));
                window.setImage("black circle", a, y);                    
            }
        }
    }
    

    public static void drawRect(int x1, int y1, int x2, int y2) {
        // Quicker way to finding minimum and maximum using Java library.
        int xMin = Math.min(x1, x2);
        int xMax = Math.max(x1, x2);
        int yMin = Math.min(y1, y2);
        int yMax = Math.max(y1, y2);
        
        if(yMin == yMax) {
            // We only have to draw a vertical line!
            for(int x = xMin; x <= xMax; x++) {
                window.setImage("black circle", x, yMin);
            }
            
        } else {
            // We have to draw to vertical lines (possibly of length 1, which
            // would be a horizontal line of length 2 in the end).
            for(int y = yMin; y <= yMax; y++) {
                window.setImage("black circle", xMin, y);
                window.setImage("black circle", xMax, y);
            }
            
            // Now for the horizontal lines. Note the indices here, to avoid
            // drawing at points already drawn. This means, if (xMax-xMin<2), 
            // then the loop will not be run.
            for(int x = xMin + 1; x < xMax; x++) {
                window.setImage("black circle", x, yMin);
                window.setImage("black circle", x, yMax);
            }
        }
    }
    
    
    public static void drawCircle(int mx, int my, int radius) {
        // General idea: To draw a circle, we check for every possible point
        // in the rectangle from (mx-radius, my-radius) to (mx+radius, my+radius),
        // whether it should be drawn. Whether it should be drawn depends on
        // whether the point lies in the _area_ of a _ring_.
        //
        // Optimisation: Instead of comparing the square root of x with the
        // square root of y, we just compare x with y, which will bring about
        // the same results for greater-than, smaller-than, and equal-to
        // comparisions.
        
        // We use some math magic, to make the circles look nicer and have 
        // the right line width.s
        final double radiusOuterSquare = Math.pow(radius + 0.5, 2);
        final double radiusInnerSquare = Math.pow(radius + 0.5 - 0.5 * Math.sqrt(2), 2);
        double distSquare;
        
        for(int y = my - radius; y <= my + radius; y++) {
            for(int x = mx - radius; x <= mx + radius; x++) {
                distSquare = Math.pow(x - mx, 2) + Math.pow(y - my, 2);
                
                if(radiusInnerSquare < distSquare && distSquare <= radiusOuterSquare) {
                    window.setImage("black circle", x, y);
                }
            }
        }
    }
}
