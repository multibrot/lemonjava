package lesson02;

import java.util.Random;

import resources.LemonWindow;

public class VisualMaze {
    
    public static int GRID_WIDTH = 16;
    public static int GRID_HEIGHT = 10;

    public static void main(String[] args) {
        
        LemonWindow window = new LemonWindow("Visual Maze", GRID_WIDTH, GRID_HEIGHT);
        Random random = new Random();
    
        for(int y = 0; y < GRID_HEIGHT; y++) {
            for(int x = 0; x < GRID_WIDTH; x++) {
                if(random.nextBoolean()) {
                    window.setImage("slash", x, y);
                } else {
                    window.setImage("backslash", x, y);
                }
            }
        }
        
        window.show();
    }

}
