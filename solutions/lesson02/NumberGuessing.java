package lesson02;

import java.util.Random;
import java.util.Scanner;

public class NumberGuessing {

    public static final int MAX_NUMBER = 100;
    
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        int number = random.nextInt(MAX_NUMBER) + 1;
        int guess = 0;
        
        System.out.println("I'm thinking of a number between 1 and 100.");
        System.out.println("What is my number?");
        
        while(guess != number) {

            guess = scanner.nextInt();

            if(guess < number) {
                System.out.println("My number is bigger.");
            }
            
            if(guess > number) {
                System.out.println("My number is smaller.");
            }
        }
        
        scanner.close();
        System.out.println("Correct! That's my number.");
        System.out.println("Thanks for playing! :-)");
    }
}
