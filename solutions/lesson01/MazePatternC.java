package lesson01;


public class MazePatternC {

    static final int MAZE_WIDTH = 20;
    static final int MAZE_HEIGHT = 6;
    
    public static void main(String[] args) {
	
	for(int y = 0; y < MAZE_HEIGHT; y++) {
	    for(int x = 0; x < MAZE_WIDTH; x++) {
		
		if((x + y) % 4 == 0) {
		    System.out.print('/');
		} else if((4 + x - y) % 4 == 1) {
		    System.out.print('\\');
		} else {
		    System.out.print(' ');
		}
	    }
	    
	    System.out.println();
	}
    }
}
