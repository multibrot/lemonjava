package lesson01;

import java.util.Random;


public class Maze {

    static final int MAZE_WIDTH = 60;
    static final int MAZE_HEIGHT = 20;
    
    public static void main(String[] args) {
	Random random = new Random();
	
	for(int y = 0; y < MAZE_HEIGHT; y++) {
	    for(int x = 0; x < MAZE_WIDTH; x++) {
		
		if(random.nextBoolean()) {
		    System.out.print('/');
		} else {
		    System.out.print('\\');
		}
	    }
	    
	    System.out.println();
	}
    }
}
