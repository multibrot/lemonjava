package lesson01;


public class MazePatternB {

    static final int MAZE_WIDTH = 20;
    static final int MAZE_HEIGHT = 6;
    
    public static void main(String[] args) {
	
	for(int y = 0; y < MAZE_HEIGHT; y++) {
	    for(int x = 0; x < MAZE_WIDTH; x++) {
		
		if(y % 2 == 1 && x % 2 == 1) {
		    System.out.print(' ');
		} else if((x + y) % 2 == 0) {
		    System.out.print('/');
		} else {
		    System.out.print('\\');
		}
	    }
	    
	    System.out.println();
	}
    }
}
