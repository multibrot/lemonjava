package lesson04;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;

public class SafeNumberGuessing {

    public static final int MAX_NUMBER = 100;
    
    
    public static void main(String[] args) {

        // This line was added as general header.
        System.out.println("Welcome to the number guessing game!");

        try {
            playGame(MAX_NUMBER);
            
        } catch(NoSuchElementException ex) {
            System.out.println("Ouch, that hurt! :-(");
        }
        
        // This line was imported from playGame().
        System.out.println("Thanks for playing! :-)");
    }
        
    
    /*
     * Play the number-guessing game, without printing "hello" or "goodbye".
     * 
     * When no data can be read anymore from the command line, the method 
     * throws a NoSuchElementException exception.
     * 
     * (This method could also be written without the parameter and a
     * hardcoded value. However, as the highest number is clearly a parameter
     * to the game, it makes sense to reflect this in the method signature, 
     * too.)
     */
    public static void playGame(int maxNumber) throws NoSuchElementException {
        
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        int number = random.nextInt(maxNumber) + 1;
        int guess = 0;
        
        System.out.println("I'm thinking of a number between 1 and 100.");
        System.out.println("What is my number?");
        
        while(guess != number) {

            //
            // new code starts here
            //
            
            guess = 0;
            
            while(guess == 0) {
                
                try {
                    guess = scanner.nextInt();
                    
                } catch(InputMismatchException ex) {
                    scanner.next();
                    System.out.println("Sorry! I did not understand. Please try again.");
                }
            }
            
            //
            // new code ends here
            //

            if(guess < number) {
                System.out.println("My number is bigger.");
            }
            
            if(guess > number) {
                System.out.println("My number is smaller.");
            }
        }
        
        scanner.close();
        System.out.println("Correct! That's my number.");
    }
}
