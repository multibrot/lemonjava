package lesson04;

import java.util.Random;

import resources.LemonWindow;


public class GameOfLifeSolved {

    public static void main(String[] args) {
        
//        int[][] state = createRandomState(20, 15);
        boolean[][] state = createGliderState(20, 15);

        LemonWindow window = new LemonWindow("Game of Life", state[0].length, state.length);
        
        updateWindow(state, window);
        window.show();
        
        for(int i = 0; i < 100; i++) {
            state = nextState(state);
            updateWindow(state, window);
            LemonWindow.sleep(250);
        }
        
        window.hide();
    }
    

    private static boolean[][] createRandomState(int width, int height) {
        boolean[][] state = new boolean[height][width];
        Random random = new Random();
        
        for(int y = 0; y < height; y++) {
            for(int x = 0; x < width; x++) {
                state[y][x] = random.nextBoolean();
            }
        }
        
        return state;
    }


    private static boolean[][] createGliderState(int width, int height) {
        boolean[][] state = new boolean[height][width];
        
        // In Java, everything variable that is not explicitly initialised 
        // has zero or null value.
        
        // This is the pattern of a glider, in Game of Life.
        state[3][4] = false; state[3][5] =  true; state[3][6] = false;
        state[4][4] = false; state[4][5] = false; state[4][6] =  true;
        state[5][4] =  true; state[5][5] =  true; state[5][6] =  true;
        
        return state;
    }

    
    private static boolean[][] nextState(boolean[][] state) {
        boolean[][] newState = new boolean[state.length][state[0].length];
        int neighbours;
        
        for(int y = 0; y < state.length; y++) {
            for(int x = 0; x < state[y].length; x++) {
                neighbours = countNeighbours(state, x, y);
                
                // Here we implement the rules of Game of Life.
                
                if(state[y][x]) {
                    if(neighbours == 2 || neighbours == 3) {
                        // "Any live cell with two or three live neighbours 
                        // lives on to the next generation."
                        newState[y][x] = true;
                    } else {
                        // "Any live cell with fewer than two live neighbours 
                        // dies, as if by underpopulation."
                        // "Any live cell with more than three live neighbours 
                        // dies, as if by overpopulation."
                        newState[y][x] = false;
                    }
                } else {
                    if(neighbours == 3) {
                        // "Any dead cell with exactly three live neighbours 
                        // becomes a live cell, as if by reproduction."
                        newState[y][x] = true;
                    } else {
                        newState[y][x] = false;
                    }
                }
            }
        }
        
        return newState;
    }
    

    private static int countNeighbours(boolean[][] state, int x, int y) {

        // This version of countNeighbours() uses the so-called ternary
        // operator. The syntax is "a ? b : c". Brackets are not strictly 
        // necessary, but that depends on what other operators are used and 
        // what priorities those operators have). "a" is a condition, "b" and
        // "c" are two values. If "a" is true, the whole expression evaluates 
        // to "b", otherwise to "c". For example, "42 > 100 ? 5 : 77" 
        // evalutes to 77.

        // loX is x-1, except x is zero, then loX should be the highest value
        // of the dimension. hiX is x+1, but if it already is the highest
        // value, then it should be 0. The same goes for loY and hiY. In 
        // other words, we wraparound the index. 
        
        int loX = (x > 0 ? x - 1 : state[0].length - 1);        
        int loY = (y > 0 ? y - 1 : state.length    - 1);

        int hiX = (x < state[0].length - 1 ? x + 1 : 0);
        int hiY = (y < state.length    - 1 ? y + 1 : 0);
                
        // Only count neighbouring cells, but not the cell itself.
        // Sometimes additional spaces can make code more readable.
        return
                (state[loY][loX]?1:0) + (state[loY][x]?1:0) + (state[loY][hiX]?1:0) +
                (state[  y][loX]?1:0) +                   0 + (state[  y][hiX]?1:0) +
                (state[hiY][loX]?1:0) + (state[hiY][x]?1:0) + (state[hiY][hiX]?1:0);
    }


    private static int countNeighbours_Variant(boolean[][] state, int x, int y) {
        int count = 0;
        
        // A different way of counting neighbours using for-loops. Note that
        // for-loops might be overkill in this case, as this variant is a bit
        // more complicated and maybe less readable. Also, while changing the
        // value of the running variables (ix, iy) is technically possible,
        // it is sometimes considered bad style as by convention the reader
        // of foreign code might expect a running variable to not change its
        // value within the loop body.

        for(int iy = y - 1; iy <= y; iy++) {
            for(int ix = x - 1; ix <= x; ix++) {

                // Make sure running variable iy stays within boundaries. 
                
                if(iy == -1) {
                    iy = state.length - 1;
                }
                
                if(iy == state.length) {
                    iy = 0;
                }

                // Make sure running variable ix stays within boundaries.
                
                if(ix == -1) {
                    ix = state[0].length - 1;
                }
                
                if(ix == state[0].length) {
                    ix = 0;
                }
                
                // Add to count, if cell is alive and neighbour.
                if(iy != y && ix != x && state[iy][ix]) {
                    count++;
                }
            }
        }
        
        return count;
    }


    private static void updateWindow(boolean[][] state, LemonWindow window) {
        // Translate game state into window images.
        for(int y = 0; y < state.length; y++) {
            for(int x = 0; x < state[y].length; x++) {
                if(state[y][x]) {
                    window.setImage("white circle", x, y);
                } else {
                    window.setImage("black circle", x, y);
                }
            }
        }
    }

}
