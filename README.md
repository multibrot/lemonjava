# Lemon Java

When Java gives you lemons, make lemonade.

Learning Java can be hard at times. Learning programming can be hard at times. Learning to code with Java without some motivating, extrinsic rewards can be daunting.

## So what's all this then?

This is a brief programming tutorial using the example of Java. The lessons here are intended to be educational and at least somewhat entertaining. However, this is not a replacement for learning the theory. There are many good books and free ebooks for that. We will focus on getting programming practise by nice little tasks here. 

A bit of previous knowledge is expected, such as: What is a loop, how do functions look like, what are variables. The beginner stuff you heard of and used a bit, but maybe not completely understood. 

You hated programming, when you had to learn it, but now you want to, but can't get going? You need a refresher? You come from a different language and need a kickstart with Java? Give it a shot!

## Show me what you got.

We're all busy, right? The lessons are intended to be done weekly, next to whatever you're doing as primary occupation in your life. We start off gently. Lessons will loosely build on each other. Because of the background of this mini-course, the workload of each lesson will vary. There will be assignments and solutions.

Here's what's in the mix:

- [Lesson 1](lessons/lesson01/): Creating a quick maze.
- [Lesson 2](lessons/lesson02/): Creating a quick maze, in pretty. Also: Number guessing.
- [Lesson 3](lessons/lesson03/): Sorting problems and pretty shapes
- [Lesson 4](lessons/lesson04/): Exception handling and the Game of Life
- [Intermezzo 1](lessons/intermezzo01/): Drama generator
- [Lesson 5](lessons/lesson05/): File IO and object orientation

## About this one thing...

Originally this mini-course was made for a one-person audience. You have some suggestions for improvement? Please let me know!

