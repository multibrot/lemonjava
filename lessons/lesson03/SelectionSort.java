package lesson03;

import java.util.Arrays;
import java.util.Random;

public class SelectionSort {
    
    public static void main(String[] args) {

        // Use fixed set of values ...
        int[] values = {84, 56, 90, 46, 50, 88, 18, 35, 28, 96};
        
        // ... or use randomly generated values instead.
        //int[] values = generateArray(10);
              
        // Show unsorted array.
        System.out.println(Arrays.toString(values));
        
        // Sort array.
        selectionSort(values);
        
        // Show sorted array.
        System.out.println(Arrays.toString(values));
    }
    
    
    public static void selectionSort(int[] values) {

        // TODO your code here
        
    }
    
    
    public static int[] generateArray(int length) {
        Random random = new Random();
        int[] array = new int[length];
        for(int i = 0; i < array.length; i++) {
            // let all numbers have two digits
            array[i] = random.nextInt(89)+ 10; 
        }
        return array;
    }
}
