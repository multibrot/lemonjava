package lesson03;

import resources.LemonWindow;

public class FunnyGeometry {

    private static int GRID_WIDTH = 20;
    private static int GRID_HEIGHT = 15;
    
    private static LemonWindow window;
    
    
    public static void main(String[] args) {
        window = new LemonWindow("Funny Geometry", GRID_WIDTH, GRID_HEIGHT);
        clearGrid();
        drawFilledRect(1, 10, 5, 12);
        drawRectNaive(0, 0, 4, 5);
        drawRect(2, 2, 14, 8);
        drawLine(9, 6, 13, 14);
        drawLine(11, 5, 19, 2);
        window.show();
    }
    
    
    public static void clearGrid() {
        
        // TODO Fill the whole grid with "white circle"s.
    }
    

    public static void drawFilledRect(int x1, int y1, int x2, int y2) {
        
        // TODO Fill rect defined by the two points (x1, y1) and and (x2, y2) 
        // with "black circle"s. Make sure your code also works, even if 
        // (x1, y1) is not left or top of (x2, y2).
    }


    public static void drawRectNaive(int x1, int y1, int x2, int y2) {
        
        // TODO Copy-paste the code from drawFilledRect() and change it such
        // that the drawn rect is not filled (i.e. only the outline is drawn).
    }


    public static void drawLine(int x1, int y1, int x2, int y2) {
        
        // TODO Draw a line from point (x1, y1) to point (x2, y2) with "black 
        // circle"s. You might want to review graphs based on linear 
        // equations. You might want to distinguish two general cases.
    }
    

    public static void drawRect(int x1, int y1, int x2, int y2) {
        
        // TODO (BONUS) Draw the outline of a rect, again, but do not use a 
        // loop within another loop.
        // (BONUS-BONUS) Make sure for each point you draw, you only draw
        // once. (I.e. don't call setImage() twice for the same (x, y) pair.
    }
    
}
