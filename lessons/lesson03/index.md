
# Lesson 03: Sorting problems and pretty shapes

This lesson will have two parts again. The first one will be educational and the second one... ehhh visual.

## Part 1: Let's sort this out 

In the first part we will talk about sorting algorithms. Writing any non-trivial program without arrays or array-like data structures is virtually impossible. Additionally, in many cases work is not done with collecting elments into an array, but usually the right element needs to be selected from the array. So often some form of look-up or finding an element in an array is necessary. Now the thing is: Element-lookup can be much faster, if the array is sorted. 

For example, recall the number-guessing game from last week. You have to find a certain number based on some criterion, but you don't know exactly which one. You "know it, when you see it" (or the other player says "Guessed correctly!"). Since you have hints available like "higher" or "lower", you can come up with a systematic approach to find the number, which reduces the steps to find the number dramatically. 

This is like searching for an element in a sorted array. In an unsorted array you would not have hints, so you basically have to guess each number until you find the right one. "Is it 1?", "No.", "Is it 2?", "No.", and so on. Basic statistics tell us that for trying many games (with a number maximum of 100) it would take 50 steps on average to guess the right number. Such a game would be no fun for humans. Similarly, our processor would not be amused, if it would have to dig through wealths of unsorted arrays when searching for elements.

### Ok, shut up, get to the point 

Our goal now is to write an algorithm that sorts an array. Luckily, there were smart brains who came up with smart ideas in the past. There are many well-known algorithms to do this. Some are better, some are worse, some are complicated, some smell like unicorn farts. Here we consider the so-called Selection Sort, which is fairly okay to understand. #YMMV 

The Selection Sort algorithm follows the basic idea, that the minimum element is _selected_, _sorted_ in, and this is repeated until the array is sorted. More precisely, we repeate the following steps a certain number of times: Among all _remaining unsorted_ elements we search for the _current minimum_. The _current minimum_ is then swapped in place with the first of the _remaining unsorted_ elements. Looking at an example can make things clearer.

    arr[] = 64 25 12 22 11
    
    // Find the minimum element in arr[0...4]
    // and place it at beginning
    11 25 12 22 64
    
    // Find the minimum element in arr[1...4]
    // and place it at beginning of arr[1...4]
    11 12 25 22 64
    
    // Find the minimum element in arr[2...4]
    // and place it at beginning of arr[2...4]
    11 12 22 25 64
    
    // Find the minimum element in arr[3...4]
    // and place it at beginning of arr[3...4]
    11 12 22 25 64 

![A visualisation of the Selection Sort algorithm.](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Selsort_de_0.gif/250px-Selsort_de_0.gif)

So yeah, this is what is going to be done. Implementing the algorithm, based on the general idea, a sketched version of the algorithm, the example above and the visualisation. While this does not seem to be a lot, often one does not start with more, when given the task to solve a problem. Hence it is important to understand the problem really well and, if needed, create helpful resources oneself. For algorithms like this it is also common to do the "pencil test", as done above: Writing down step by step how the array should look like after each loop iteration. 

Note: While this example is taken from Wikipedia (English text, German image), going there (or anywhere Your Favourite Internet Search Engine suggests) will quickly reveal the coded algorithm. Looking at solutions is only half the fun, obviously.

### I wants teh real codez

In this folder there is a Java-file with a basic template prepared. All we have to do is to implement the following method based on the ruminations above:

    public static void selectionSort(int[] values) {
        // TODO your code here
    }

An `int[]` array as argument and no return value means that we have to modify the elements of the `values` array. That is, we write into the place where we read from.

Swapping two elements in an array means: If there is `a[i]==x` and `a[j]==y`, make it so that `a[i]==y` and `a[j]==x`, assuming `x!=y` of course. You will need a "tool" do actually do this in code.

### Bonus task: Print it yourself!

In the provided code scaffold, the following code is used to print the `int[]`-array `values`:

    System.out.println(Arrays.toString(values));
    
Here the method `Arrays.toString()` from the Java library is used, to turn an `int[]`-array into a `String` value. We observed how that output looks like. However, we could do that ourselves! Try to write a method, that takes an `int[]`-array as argument and returns a `String`, so it looks like the output from `Arrays.toString()`. Remember, you can concatenate two strings with a `+`, for example:

    String message = "I " + '<' + 3 + " ducks.";

## Part 2: Pretty shapes

This part will be visual again. We are going to draw stuff! These are some basics that underly programs like MS Paint and maybe the first versions of Photoshop. (Adobe now probably uses dark magic everywhere instead.)

The supplied Java-file already has a `main`-method, where some calls to methods are made, but the methods are missing. Our job is to implement those missing methods. The methods are sorted by their difficulty and each can in principle be tackled indepedently of the others.


*   `public static void clearGrid()`
        
    Use the `setImage()` method for every possible field in the grid and set it to `"white circle"`. The dimensions of the grid are defined as class-wide variables.    

*   `public static void drawFilledRect(int x1, int y1, int x2, int y2)`
    
    Fill all the fields of the grid, that belong to the given rectangle, to `"black circle"`. The rectangle is defined by the two points `(x1, y1)` and `(x2, y2)`.
    
    Note, it is possible that the first point is not top-left of the second point. Make sure, that your code also works for such cases (i.e. don't rely on `x1 < x2` and `y1 < y2`). This is also true for the following methods.

*   `public static void drawRectNaive(int x1, int y1, int x2, int y2)`
    
    There are at least two ways to draw a rectangle that is not filled, but only the outline. Here we aim for the easier one. The code can be based on `drawFilledRect()` (copy-paste in this case) and checks, whether some points should be drawn or not.
    
*   `public static void drawLine(int x1, int y1, int x2, int y2)`
         
    Draw a line from point `(x1, y1)` to point `(x2, y2)` with `"black circle"`s. 
    
    This one might be a bit tricky. You might want to review graphs based on linear equations. That is: Having a y-value dependent on the x-value or vice versa. Especially the formula that defines a line if two points are given, might come in handy here.   
    
    You might want to distinguish two general cases, so the dependency of one variable on the other works out as intended.
    
    **Bonus**: If we want to aim high, we can also care about doing the math not with integers, but floating-point variables (`float` or `double`) and possibly use `Math.round()` (and convert back to integer with `(int) floatValue`). This makes calculations more precise, but also makes the code slightly more complicated.

*   `public static void drawRect(int x1, int y1, int x2, int y2)`
        
    Draw a rectangle, again. This is a **bonus exercise**, but maybe easier than `drawLine()`, depending on your taste. Here we aim for a more efficient approach, which does _not_ use two nested loops (though multiple loops are okay). That is: Only iterate over the points you actually want to draw.
    
    **Bonus bonus**: An important optimisation here is to make sure that no point gets drawn twice.
   
When everything works out properly, the the output should look like on the following image. (Your lines from `getLine()` might look slightly different, depending on your implementation.

![Expected output of all that drawing madness.](funny-geometry.png)

### Honourable mentions

If you're really curious you can check the end of the solutions-file for an implementation of a `drawCircle()` method, given a coordinate and a radius. It is actually not too difficult in principle, but optimisations are always possible of course. The `drawCircle()` method might be interesting, because one needs to think a bit differently about the problem. 