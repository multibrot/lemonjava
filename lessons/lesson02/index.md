
# Lesson 02: Creating a quick maze, in pretty. Also: Number guessing.

This week will have two parts. Something more visual and something educational. We gonna have dessert first, but the other half is also nice.

## Part 1: The visual stuff

First we will pimp the maze generator from last week a little. In fact we keep the general strcture, but replace a few lines to get prettier output. To do this we use the supplied class `LemonWindow`, which will allow us to create a graphical interface with minimal effort. There are setup instructions below.

### Partial documentation of `LemonWindow`

The `LemonWindow` class has a couple of methods, but only the following are needed in this case.

* `public LemonWindow(String title, int columns, int rows)`

    The constructor for the window. The window title is given by _title_ and can be arbitrary. Note that recent versions of Windows may not always display the window title. 

    The window shows a grid of cells, where the dimension of the grid are specified by _columns_ and _rows_. At the moment, a cell has a size of 32x32 pixels.

* `public void setImage(String imageID, int x, int y)`

    Set the cell in the grid at position (_x_, _y_) to display the an image identified by _imageID_. Note that _imageID_ is not a path, but a given identifier. More on those identifiers will be covered at a later point in time. For now, the identifiers `slash` and `backslash` are accepted.

* `public void show()`

    Display the window. Yes, the window won't do that by itself.

### So what to do?

Take the code for maze generation from last week as the base and get rid of all `print*()` statements. Build a `LemonWindow` instance in the beginning and use `show()` in the end. For each grid cell, use the `setImage()` method.

### Additional files and setup

Beware that apart from the file `LemonWindow.java` you will need an additional directory with image files, which can be found in this code repository at `/lessons/resources`. The easiest ways to get this is to either download the full code repository from Github or by using Git, which is smarter on the long run. Either can be done via the blue "Clone" button in the main view of the repository, or the button next to it.

Depending on your local setup, there might be problems loading files. Specifically you might encounter the error `Image not found for path...`. The files should be found, when your local directory structure mirrors the one from this code repository. If you are using a Java IDE such as Eclipse, you might need to refresh your project (right click, "Refresh") and/or set the `lessons` directory as _source folder_ in the _Java Build Path_  settings of your project.

## Part 2: The educational stuff

For the rather educational part this week we look at the famous "number guessing game". The game is a classical programming problem (and I believe every programmer should have solved it once, like printing "hello world"). 

The game goes like this: One player (the computer) is thinking of a number between 1 and (say) 100 and the other player has to guess that number (surprise!). The first player either confirms the guess or states whether the number is too high or too low.

For example, this could look like this:
    
    I'm thinking of a number between 1 and 100. What is my number?
    43
    My number is bigger.
    76
    My number is bigger.
    84
    My number is bigger.
    92
    My number is smaller.
    90
    My number is bigger.
    91
    Correct! That's my number. Thanks for playing! :-)

### And I have to code that now?

Yep. Try to reproduce the program behaviour above (the output without the lines with only numbers! ;-). That is: Have some introductory message and then read in a number. If that guess is to high or too low, print an appropriate message and again read a number. If the guess matches the desired number, print an appropriate message and end.

To read a number from the terminal, you can use the `Scanner` class roughly like this:

    int guess;
    Scanner scanner = new Scanner(System.in);
    guess = scanner.nextInt();

To generate the number to be guessed, you can use the `nextInt()` method of the `Random` class. Use the specific method, which takes one parameter.

There are multiple ways to write the number guessing game, but the basic structure (in terms of necessary loops and conditionals) will be the same for most of them. That means that the basic structure can be derived from how the game works, as stated above. Therefore this is more an exercise of capturing the basic algorithm of the game. Working out the algorithm is something not done with code, but with a brain. In other words: The hard part of this exercise can be done without writing code. This matters, as trying to think "with the hands" can sometimes lead to some trial and error, that could be avoided.

### Bonus questions

The questions here are touch more on the theoretical aspects of the number guessing game and hint at the area of theoretical computer science.

* What is the optimal strategy to guess the number? 
* If 100 is the maximum number that can be guessed, how many steps are necessary at most to guess the number with the optimal strategy?
* Can you think of a mathematical formula to express the maximum number of steps depending on the maximum number that can be guessed?
