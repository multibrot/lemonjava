
# Lesson 01: Creating a quick maze.

## What is it about?

In this first lesson we warm up a little and create a random maze very minimal effort. The idea comes from a [one-liner written in BASIC](https://www.slate.com/articles/technology/books/2012/11/computer_programming_10_print_chr_205_5_rnd_1_goto_10_from_mit_press_reviewed.html). On that webpage is a link that shows that the maze there can actually look quite nice. Our maze will look like the one below.

    \\\\\///\\/////\\//\//\//////\//\//\/\\//\\\\/\/\/\/\/\//\//
    /\\/\\\\\\//\\/\\\///\//\/////\\/\\\\\\\\\\/\/\//\//\\\///\/
    \/\//\/\\\\/\\///\\/\//\/\\\\//\////\///\/\/\\/\/\/\\\////\/
    /\\\\\\/\////\\\//\//\\\/\/\\\/\\/\\\\\\\/////\/\\\\\\\\\\\/
    ////////\\\///\/\\\/\\\\/\\/\\\\//\//\\/\/\\\\//\\\/\\\\///\
    //\/\\/\\//\\\\\//\//\\///\/\\//\/\\\\\//\\/\\/////\///\\//\
    \\//\/\///////\///////\/\\\\\\//\/\//\\\\/\\\/\///\/\///\/\/
    \\/\/\/\\\\\\///\///\/\\/\\/\\\\///\/\\\\\///\\/\\\/\\/\\///
    \/\\\\/\/\/\/\\/\\/\\\/\\///\\\/\//\/\/\\/\\//\//\///\\\\///
    /\\\\/\\/\/\\//\/\\/\//\\\/\\/\//\/\//\//\\////\///\\/\//\/\
    \\\\/\\///\\\//\///\\/\\\//\/\\/\\/\/\//\\/\\\////\\\/\////\
    ///\\\\/\\\//\\/\\//////\////\/\//\\/\/\//\/\\//\\\/\\\\\/\\
    /\/\\\\\\//\\\//\////\/\\/\\/\//\\///\\////\\//\/\\\\\///\\\
    //\/\\\\//\\/\///\/\\\/\\/////////\\\/\\\\//\\//\\//\\\\\\//
    //\\\\\\\/\//\/\\\////\/\\\\\\//\///////\/\\//\\\//\//\/\\\\
    ///\\\\\/////\//\//\//\///\\\\\\/\//\//\\\//\/\\\\//\/\\///\
    /\/\/\/\///\/\\\//\\\//\\//\\\//////\\/\\////\//////\/\\//\\
    /\/\\/\///\//\/\/\\//\/\/\\\//\/////\\/\//\/\\\\\\/\/\\////\
    \/\/\//////\////\////////\\\\\\/\\///\/\\\\\/\///\\\\/\/\/\/
    \//\\\/\\/\/\\\\////\/\\\/\//\/\\\\/\\/\///\\\//\/\\\\\//\\/

Yes, okay, there is no strict start and exit, and a path from entrance to exit is not guaranteed. And you might get your head stuck at 45 degree, if you stare at it for too long. And it looks much better, if there is no line spacing. But we could work on that when we have a good basis. We just care about the basis for now. That is: Creating a simple maze with minimal effort.

## Instructions

Create a Java class and a `main` method as usual. On class scope, define two variables for the width and height of the maze with values of your liking. Then try to create output like the one above.

Since the maze should have some randomness in it, you will need Java's `Random` class. Create only one instance. To get a random boolean value, use the instance method `nextBoolean()`.

## Bonus instructions

Instead of random maze creation, you can try to recreate the following patterns. You will need to look closely at the pattern and how each symbol repeats. Java-wise, the modulo operator (`%`) and logical operators (`&&` or `||`) will be needed. For example, to check whether you are printing a character in an odd row, you could write:

    if(index_of_row % 2 == 1) {
        /* ... */
    }

Bear in mind that this is more of a puzzle and less of a coding exercise. You train your eye and analytical thinking, which is also a good thing. But it can be as joyful or frustrating as solving a sudoku puzzle. Better stop, if it's not fun anymore.

**Pattern A: diamonds**

    /\/\/\/\/\/\/\/\/\/\
    \/\/\/\/\/\/\/\/\/\/
    /\/\/\/\/\/\/\/\/\/\
    \/\/\/\/\/\/\/\/\/\/
    /\/\/\/\/\/\/\/\/\/\
    \/\/\/\/\/\/\/\/\/\/

**Pattern B: bricks**

    /\/\/\/\/\/\/\/\/\/\
    \ \ \ \ \ \ \ \ \ \
    /\/\/\/\/\/\/\/\/\/\
    \ \ \ \ \ \ \ \ \ \
    /\/\/\/\/\/\/\/\/\/\
    \ \ \ \ \ \ \ \ \ \

**Pattern C: big diamonds**

    /\  /\  /\  /\  /\
      \/  \/  \/  \/  \/
      /\  /\  /\  /\  /\
    \/  \/  \/  \/  \/
    /\  /\  /\  /\  /\
      \/  \/  \/  \/  \/

**Hints (in [ROT13](https://rot13.com/))**

* gel nqqvat vaqvprf sbe ebj naq pbyhza.
* gur beqre bs vs-fgngrzragf znggref. fb qbrf jung flzoby vf cevagrq va gur ryfr-oybpx.
* pna or qbar jvgu bayl bar cevag-fgngrzrag cre flzoby (vapyhqvat fcnpr).
