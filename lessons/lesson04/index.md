
# Lesson 04: Exception handling and the Game of Life

We have two parts again. The first one is a bit more conceptual and intended to be short in terms of required time, so there there is more time for the second, bigger part.

## Part 1: Exceptional code

### Some theory

Exception handling is an important part of any modern language. It often happens that some code runs and something happens that violates certain assumptions, such that it does not work to run the subsequent lines of code. 
For example, let's say we want to read the content of a file and print the interesting parts of it. Now it could happen that the path we specified to open the file does not actually point to a file. In that case it does not make sense to run the code for printing. (No data to print.)

This is an exceptional case and we want to handle this in a smooth way. What are our options? We could handle such a situation with an if-statement (by checking whether a file exists before opening it). In many cases you need to do such checking a couple of times, but that will make the code hard to read, since functional code is interleaved by error-handling code. (Code written in C for example could look like this.) 
To avoid this, people came up with a language construct to deal with that, which is now standard in most modern programming languages (Like Java or also C++.) That could look roughly like this in Java:

    // some code
    
    try {
        feedTheDucks();
        doDangerousStuff();
        feedTheUnicorns();
        
    } catch(EverythingIsBrokenException ex) {
        System.out.println("Oh no! Something bad happened! The error was:");
        System.out.println(ex.getMessage());
        System.out.println("Stopping feeding, continuing with rest.");
    }
    
    // the rest

Java will try to execute the four methods in the `try`-block. If nothing goes wrong there, the code in the `catch`-block is never executed. But let's say _something bad_ happens in the `doDangerousStuff()` method. Java will stop executing code and "jump out" of every block and method it is in at the moment. Java does this until it exits the `main()` methods, then prints the typical error messages (and stack traces) you know by know, and then terminates. 

To be more precise, the _something bad_ is understood here as a so called _exception._ Exceptions are _thrown_ when they arise. If you know the type of a thrown exception, then you can _catch_ it. In the example above it is implied that `doDangerousStuff()` could throw an exception of the type `EverythingIsBrokenException`, which could then be caught by that specific `catch`-block. This will make the program to continue, although `feedTheUnicorns()` then is never executed. (If `doDangerousStuff()` would throw an exception of a different type, it would be _uncaught_ in this example and again the program would crash.)

### Recommended reading

This is a very brief intro to exception handling and not complete. Many clever brains have written good introductions on this. One of the great, popular, and free ebooks on Java is [Thinking in Java](https://archive.org/details/thinkinginjavaeckel) by Bruce Eckel. There is a [slightly older version](https://archive.org/details/ThinkingInJava3rdEdition) available, too, which allows a direct PDF-download.
In this context it is recommended to read the following sections of chapter 9 on "Error Handling  with Exceptions", which should be about 3.5 A4 pages.

- Basic exceptions
- Catching an exception
- The exception specification

### Applying what we learned

Let's recall our number-guessing game from Lesson 2. There we asked the used to input an integer and used code like to following:

    Scanner scanner = new Scanner(System.in);
    int guess = 0;
    
    // ...
    
    guess = scanner.nextInt();

However, when we were goofing around and entered something Java could not interpret as integer, we faced an `InputMismatchException` and the game stopped brutally. That's not nice. Let's copy the code from Lesson 2 and improve that.

-   **[code]** `nextInt()` can throw an `InputMismatchException`, if invalid input is provided. If the user is entering nonsense, we want the program to comment on that and offer the user again to enter something. We actually want to be sure we can continue with sane input, so let's integrate a try-catch-statement and bug the user until valid input is entered.

    Note that when `nextInt()` exited unsuccessfully, the user input that is held by the `Scanner` instance is still there. If we would just call `nextInt()` again, it would not try to get for new input from the user, but use the existing, not-yet-handled input (that already caused problems). To get rid of that input, we jProblemust get it with the `next()` method and ignore the output.

-   **[try]** When looking at the [Java API documentation for `nextInt()`](https://docs.oracle.com/en/java/javase/14/docs/api/java.base/java/util/Scanner.html#nextInt()) we see, that the method could also throw two other exceptions. Both can happen on some more general problems, like when we cannot use the `Scanner` instance anymore. 

    For example, the scanner cannot function anymore, when the program is notified that the _input stream_ `System.in` (from which the `Scanner` instance reads) is closed, i.e. cannot provide anymore input. People also say that [STDIN](https://en.wikipedia.org/wiki/Standard_streams#Standard_input_(stdin)) is at [EOF](https://en.wikipedia.org/wiki/End-of-file). Our guessing game however expects a steady influx of integers until the right guess is made. Hence it requires the STDIN (standard input stream) to not be at EOF (end of file). If it does however, we want to deal with this "exceptional" situation and end the program gracefully.
    
    You can provoke this situation right away. Instead of provding input when prompted, close the standard input stream by pressing `Ctrl+Z` on Windows and `Ctrl+D` on every other platform. This will cause `nextInt()` to throw an exception. (If you are running your programs within Eclipse, make sure you have focus the integrated console view before pressing the key combination.) 

-   **[code]** As such a problem situation is more general and has higher impact than just invalid input, we want to deal with it on a more general level, too. Wrapping all our existing code into another try-catch-statement is certainly possible, but makes code more complicated and hard to read. Let's be smarter and keep our methods flat, short and readable.

    Rename your `main()` method into a `playGame()` method, that takes no arguments. Then create a new `main()` method in which you call the other method. Wrap the method-call into a try-catch-statement and handle the exception you provoked above by printing a disappointed error message. 

    You will notice that the `catch`-block will not be accepted rightaway by the Java compiler, because nothing in the `try`-block looks suspicious. This is, because we need to declare that `playGame()` could potentially throw an exception (or just forwards an exception from another method call).  

-   **[revise]** As the the last point to do now, revise your two methods in their current state. Generally a method is supposed to do some specific task (sometimes also referred to as _responsibility_ in programming). The point of `playGame()` might be to only have output that is relevant for actually plaing the number-guessing game. Code or output like welcoming [ASCII art](https://en.wikipedia.org/wiki/ASCII_art) or developer credits might not belong to the essential game-playing code, but might better be placed outside of it, i.e. into the new `main()` method. Does your `playGame()` do what it is supposed to do, also in terms of print-statements? 


## Part 2: Game of Life

Next up we will implement a classic system in computer science, namely a cellular automaton. A cellular automaton consists of a state and rules that transform each state into the next state. More precisely, the cellular automaton has a number of cells, a state is the total of the values the cells have and the rules work with cell values to define new cell values.

One of the most famous celluar automata is the Game of Life by Conway. There the cells form a two-dimensional grid and each cell can bei either dead or alive. The rules, [taken from Wikipedia](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Rules) are as follows:

1. Any live cell with fewer than two live neighbours dies, as if by underpopulation.
2. Any live cell with two or three live neighbours lives on to the next generation.
3. Any live cell with more than three live neighbours dies, as if by overpopulation.
4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

From those simple rules, already pretty complex behaviour arises. This is how the Game of Life got its name.

### So this is going to happen now, right?

Yeah! This will be a bit of code to write, but don't worry, we will try to break it down into managable bites. The cool thing about the Game of Life is that each new state will derive from the previous state and since we want to see every state, we will get some form of animation to watch (if you can call it that).

First, let's discuss our `main()` method. We want to keep that one short and crisp and outsource all the heavy lifting to other methods (see below). First we want to create a new `boolean[][]` array, which will hold our state. This will be outsourced to a dedicated method, but more on that below. Then we create a `LemonWindow` with the same dimensions of the array, update it with the current state (see below) and `show()` it.

Next we repeat three steps a couple of times, say 100 iterations. First we calculate the next state of the cellular automaton, then we update the window with it, and finally we pause the whole program for a certain duration. This basically will create the animation. Pausing the program can be achieved with the method `LemonWindow.sleep(int milliseconds)`. This is a static method, so it can be called without an instance, just by writing `<class-name>.<method-name>(<parameters>)`. When we are done with all the many iterations, we `hide()` our instance of the `LemonWindow`.

### More challenge

We include two additional challenges: 

1. Don't use class-wide variables! While this would be perfectly reasonable in this case, not using them will force us to be explicit about what variables the methods below need. This makes the methods more self-contained, easier to grasp and better to test. #EducationalReasons
2. Don't store the dimensions for the `LemonWindow` instance in variables and only have them once in your code! Since we create a `boolean[][]` array and the array remembers its length via the `length` attribute, we can always get the dimensions from there. Note that a two-dimensional array is in fact an array of arrays. Think about that for a second. Array of arrays. That means, if `array` is of type `boolean[][]`, then `array.length` gives you the length of the first dimension. `array[i].length` first gives you the `i`th element of `array` (which is an array) and then gets the length of this array. Okay? :-)

### The tools we will build

Let's get to the methods we will use in this case. 

*   `void updateWindow(boolean[][] state, LemonWindow window)`

    Let's start nice and easy. We get a `state` full of dead and alive cells and want to display those in the given `LemonWindow`. That means, as we did in the past already, calling `setImage()` a couple of times. Use the image IDs `"black circle"` for alive cells and `"white circle"` for dead cells.

*   `boolean[][] createGliderState(int width, int height)`

    Next up we create the game state, i.e. a `boolean[][]` array of the size defined by the arguments. It is suggested that the first index should be the Y-dimension and the second one should be the X-dimension, which has some advantages in certain situations. Either way, always remember the order of your dimensions in the array. Also, we want the whole state to be dead cells, except for a certain figure called the "[Glider](https://en.wikipedia.org/wiki/Glider_(Conway's_Life))" in Game of Life lingo. That is, somewhere in the array you create, set neighbouring cells to alive to match the following pattern, where a black circle means an alive cell and otherwise a dead cell.
    
    ![The Hacker emblem. No kidding. Also great you're reading alt texts!](https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Glider.svg/140px-Glider.svg.png)
        
    Later we create another method to initialise the state variable randomly. However, for now it is good to create such a hardcoded state variable, as this allows us to verify that the following algorithm works, because we know what to expect.
    
    Pro tip: While Java complains when we are reading a variable before having written to it, Java will not do so in the case of arrays. Then it will automatically initialise every element in the array with a false-ish default value, i.e. `0` for `int[]`, `null` for `String`, and `false` for `boolean[]`.

*   `boolean[][] nextState(boolean[][] state)`

    The core of the cellular automaton! Here a new state will be created from the old state. This is the place where we encode the rules written above. That is, for each cell a new cell state should be procuded, based on the rules above.
    
    To apply the rules, we need to count the number of neighbours. However, we push counting into another method, so we can focus on applying the rules here. That means, for each cell, we first count the neighbours and then depending on the count we determine the next cell state according to the rules.
        
    Hint: Writing to the _same array_ you also read from (all in the same loop) can cause unexpected side effects. We want to avoid this somehow.

*   `int countNeighbours(boolean[][] state, int x, int y)`

    For a cell given by the `x` and `y` values, we want to count the number of neighbours the cell has. A neighbour of a cell are all the cells that touch it, either on an edge or at the corner.
    
    Additionally, we assume an "infinite" world that "wraps around". If for example a virtual duck would be in the right-most column of the grid and step to the right, we appear in the left-most column. (Same for all directions.) The advantage of wrap-around is that we can rely on our neighbour count to always be between 0 and 8, which makes applying the cellular automaton rules a bit easier.
    
    However, there is one issue we need to consider. If we would just something like `state[y][x - 1]`, we will get an `ArrayIndexOutOfBoundsException` for sure. So in this method we need to take care of proper index wrap-around, when we can expect indices to, well, go out of bounds. 

*   `boolean[][] createRandomState(int width, int height)`

    When we verified everything works, let's see the full game of life in action. Instead of `createGliderState()` we want to be able to use this method instead, which initialises the full array with random values. `Random.nextBoolean()` is your friend. 
