
# Lesson 05: Input and output and input and output and input

This lesson will have three parts. We will work with files and then get our hands dirty a bit more with object orientation.

## Part 1: File input and output

File input and output (file IO) is an important part of programming, as it allows to break out of the confinements of isolated programs and allows to interact with the file system or automate crazy shit. More advanced concepts like network IO are heavily inspired by files, so it is important and helpful for the aspiring softare engineer to work with files skillfully.
  
### Recommended reading

There are a lot of good introductions to working with files in Java, so let's try to work with that. 

The following sections in the book Thinking in Java ([mentioned earlier](/lessons/lesson04#recommended-reading)) are recommended theoretical knowledge for this part. The page numbers refer to the PDF-Version of the book. 

* page 328, sections:
    - The Java I/O System
    - *not* The File class (see comment below)
* pages 332-338, sections:
    - Input and output
    - Adding attributes and useful interfaces
    - Readers & Writers
* pages 343-344, sections:
    - Standard I/O
    - Reading from standard input

The File class is important and the same concept exists in virtually any modern language. However, the section on the File class might be a bit complicated at this stage. It involves some other concepts like Interfaces or more on object orientation, so it might be hard to grasp fully right now. Let's look at it in more detail at a later stage.

#### Relative and absolute paths

The edition of Thinking in Java linked above does elaborate on the difference between absolute and relative paths. However, for the exercise and in general this is surely useful knowledge. 

In short, absolute paths are strings of characters that completely where a (potential) file exists on your file system. Absolute paths start with the drive letter (`C:\` for example on Windows) or the file system root (`/` almost anywhere else). For example `C:\Users\FluffyUnicorn\Desktop\ShoppingList.txt` (on Windows) or `/home/FluffyUnicorn/Desktop/ShoppingList.txt` (almost anywhere else). 

Relative paths do not address a (potential) file by themselves, but rather they need to be understood _relative_ to an absolute path to make sense. To open a file, the operating system always needs an absolute path to file, but it can craft that path based on the parts, if necessary. For example, if the relative path `Desktop\ShoppingList.txt` is appended to the absolute path `C:\Users\FluffyUnicorn`, we can refer to the same file as above. The same is true for `Desktop/ShoppingList.txt` together with `/home/FluffyUnicorn` of course.

There are two special "pseudo-directories" that might appear in paths. Firstly, `..` refers to the parent directory. So the relative path `..\Pictures\FunnyCat.jpg` appended to the path `C:\Users\FluffyUnicorn\Desktop` gives `C:\Users\FluffyUnicorn\Desktop\..\Pictures\FunnyCat.jpg`, which is the same as `C:\Users\FluffyUnicorn\Pictures\FunnyCat.jpg`. 

Secondly, there is the special "pseudo-directory" `.`, which just refers to the same directory. So, the following paths actually all refer to the same file. (Although this seems pointless, there are reasons for this concept, but those won't be covered here.)

* `C:\Users\FluffyUnicorn\Pictures\FunnyCat.jpg`
* `C:\Users\FluffyUnicorn\.\Pictures\FunnyCat.jpg`
* `C:\Users\FluffyUnicorn\Pictures\.\FunnyCat.jpg`
* `C:\Users\FluffyUnicorn\Pictures\.\.\.\.\FunnyCat.jpg`
* `C:\.\.\Users\.\.\FluffyUnicorn\Pictures\.\.\.\FunnyCat.jpg`

### Y

I'll pat myself on the back for the choice of headline, and probably I will remain the only one doing that. However, the title visually conveys what we are going to do (at least in my mind): Merge two files into one. 

To do this, we will first start with a simpler problem, to not strain our coding muscles too much at once. Then, when the tricky part is done in less code, we adapt it to get where we want.

* **[Preparation]** Create two text files and put some lines of random text into each file.

* **[Attack 1]** Open only one of the input files and the output file with appropriate Java classes. In a loop, write each line from the input file to the output file. Close the files afterwards. Verify the output file has been written correctly. (This might sound trivial, but this provides a lot of potential pitfalls already. When this is working we will adapt the code.)
    
* **[Attack 2]** Adapt your code so both input files and the output file are opened. For each pair of input lines A and B write an output line that has the form " A + B " (without quotes). Do this as long as you can read a line from each of the input files. Close what you have opened properly.

#### Hints

* There are multiple ways to read and write files and each way involves a (slightly) different set of classes from the Java library.
* When you get errors that Java could not find files, you might want to try absolute instead of relative paths. Generally relative paths should work though. When code is ran within Eclipse for example, relative paths are based in the project root directory (which also holds the directories "bin", "src", and so on).
* Java classes to read files or streams usually have a `ready()` method (or equivalent), that returns a `boolean` and lets you know whether there is more data that can be read.
* You might want to use the `trim()` method of a `[String](https://docs.oracle.com/javase/7/docs/api/java/lang/String.html)` instance. (Generally it is wise to know the methods of that class well, as they are needed very often when manipulating strings.)

## Part 2: Object orientation

