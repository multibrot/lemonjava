package resources;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;


public class LemonWindow {

    private final int FIELD_SPACING = 0;
    private final int IMAGE_WIDTH = 32;
    private final int IMAGE_HEIGHT = 32;
    
    private final int gridColumns;
//    private final int gridRows;
    private final JFrame frame;
    private final JPanel fieldContainer;
    private final Map<String, ImageIcon> images; 
    private final List<LemonEvent> events;
    
    
    public LemonWindow(String title, int columns, int rows) {
        
        gridColumns = columns;
//        gridRows = rows;
        images = new HashMap<String, ImageIcon>();
        events = new LinkedList<>();

        // GUI
        
        frame = new JFrame(title);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        frame.addKeyListener(new KeyListener() {
            @Override public void keyReleased(KeyEvent e) {}
            @Override public void keyTyped(KeyEvent e) {}
            @Override public void keyPressed(KeyEvent e) {
                events.add(new LemonEvent(e.getKeyCode()));
            }
        });
        
        fieldContainer = new JPanel();
        fieldContainer.setBackground(Color.BLACK);
        fieldContainer.setBorder(BorderFactory.createEmptyBorder(FIELD_SPACING, FIELD_SPACING, FIELD_SPACING, FIELD_SPACING));       
        fieldContainer.setLayout(new GridLayout(rows, columns, FIELD_SPACING, FIELD_SPACING));
        
        MouseListener mouseListener = new MouseListener() {
            @Override public void mouseReleased(MouseEvent e) {}
            @Override public void mousePressed(MouseEvent e) {}
            @Override public void mouseExited(MouseEvent e) {}
            @Override public void mouseEntered(MouseEvent e) {}
            @Override public void mouseClicked(MouseEvent e) {
                Component clickedComponent = e.getComponent();
                Component[] components = fieldContainer.getComponents();
                int index = -1;
                for(int i = 0; i < components.length; i++) {
                    if(components[i] == clickedComponent) {
                        index = i;
                        break;
                    }
                }
                int button;
                switch(e.getButton()) {
                case MouseEvent.BUTTON1: button = LemonEvent.MOUSE_BUTTON_LEFT; break;
                case MouseEvent.BUTTON2: button = LemonEvent.MOUSE_BUTTON_MIDDLE; break;
                case MouseEvent.BUTTON3: button = LemonEvent.MOUSE_BUTTON_RIGHT; break;
                default: button = LemonEvent.MOUSE_BUTTON_UNDEFINED;
                }
                events.add(new LemonEvent(index % columns, index / columns, button));
            }
        };
        
        MouseWheelListener mouseWheelListener = new MouseWheelListener() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                Component clickedComponent = e.getComponent();
                Component[] components = fieldContainer.getComponents();
                int index = -1;
                for(int i = 0; i < components.length; i++) {
                    if(components[i] == clickedComponent) {
                        index = i;
                        break;
                    }
                }
                int button;
                if(e.getWheelRotation() < 0) {
                    button = LemonEvent.MOUSE_WHEEL_UP;
                } else {
                    button = LemonEvent.MOUSE_WHEEL_DOWN;
                }
                events.add(new LemonEvent(index % columns, index / columns, button));
            }
        };
        
        JLabel label;
        
        for(int i = 0; i < rows * columns; i++) {
            label = new JLabel();
            label.setPreferredSize(new Dimension(IMAGE_WIDTH, IMAGE_HEIGHT));
            label.setHorizontalAlignment(SwingConstants.CENTER);
            
            label.addMouseListener(mouseListener);
            label.addMouseWheelListener(mouseWheelListener);
            
            fieldContainer.add(label);
        }
        
        frame.getContentPane().add(fieldContainer);        
        frame.pack();
        frame.setLocationRelativeTo(null);
        
        // default images
        
        addImage("black circle", "resources/circle-black.png");
        addImage("white circle", "resources/circle-white.png");
        addImage("slash", "resources/slash.png");
        addImage("backslash", "resources/backslash.png");
    }
    
    public void show() {
        SwingUtilities.invokeLater(() -> {
            frame.setVisible(true);
        });
    }
    
    public void hide() {
        SwingUtilities.invokeLater(() -> {
            frame.setVisible(false);
            frame.dispose();
        });
    }
    
    public void draw() {
        SwingUtilities.invokeLater(() -> {
            frame.revalidate();
            frame.repaint();
        });
    }
    
    public void setImage(String imageID, int x, int y) {
        if(imageID != null && !images.containsKey(imageID)) {
            String message = String.format("Image with ID \"%s\" does not exists! :-(", imageID);
            throw new IllegalArgumentException(message);
        }
            
        SwingUtilities.invokeLater(() -> {
            JLabel label = (JLabel) fieldContainer.getComponent(gridColumns * y + x);
            label.setIcon(images.getOrDefault(imageID, null));   
            frame.invalidate();
        });

    }
    
    public void addImage(String imageID, String path) {
        if(images.containsKey(imageID)) {
            String message = String.format("Image with ID \"%s\" already exists! :-(", imageID);
            throw new IllegalArgumentException(message);
        }        
        
        URL url = ClassLoader.getSystemClassLoader().getResource(path);
        
        if(url == null) {
            String message = String.format("Image not found for path \"%s\"! :-(", path);
            throw new IllegalArgumentException(message);
        }        

        ImageIcon image = new ImageIcon(url);
        
        if(image.getIconWidth() != IMAGE_WIDTH || image.getIconHeight() != IMAGE_HEIGHT) {
            String message = String.format("Image is not in format %dx%d for image \"%s\"! :-(", IMAGE_WIDTH, IMAGE_HEIGHT, path);
            throw new IllegalArgumentException(message);
        }
        
        images.put(imageID, image);
    }
    

    public LemonEvent[] getInputEvents() {
        LemonEvent[] eventsArray = events.toArray(new LemonEvent[0]);
        events.clear();
        return eventsArray;
    }
    
    
    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    

    public static void playSound(String path) throws IOException {
        playSound(path, 1);
    }

        
    public static void playSound(String path, int times) throws IOException {
        URL url = ClassLoader.getSystemClassLoader().getResource(path);
        int loops = (times > 0 ? times - 1 : Clip.LOOP_CONTINUOUSLY);
        
        if(url == null) {
            throw new FileNotFoundException("Could not find file: " + path);
        }
        
        try {
            Clip clip = AudioSystem.getClip();
            AudioInputStream inputStream = AudioSystem.getAudioInputStream(url);
            clip.open(inputStream);
            clip.loop(loops);
            
        } catch (LineUnavailableException | UnsupportedAudioFileException  | IOException e) {
            throw new IOException("Could not play sound! " + e.getMessage(), e);
        }
    }
}
