package resources;

public class LemonEvent {
    
    public static final int EVENT_TYPE_KEYBOARD_EVENT = 1;
    public static final int EVENT_TYPE_MOUSE_EVENT = 2;

    public static final int MOUSE_BUTTON_UNDEFINED = 0;
    public static final int MOUSE_BUTTON_LEFT = 1;
    public static final int MOUSE_BUTTON_MIDDLE = 2;
    public static final int MOUSE_BUTTON_RIGHT = 3;
    public static final int MOUSE_WHEEL_UP = 4;
    public static final int MOUSE_WHEEL_DOWN = 5;

    public int eventType;
    public int mouseX = 0;
    public int mouseY = 0;
    public int mouseButton = 0;
    public int keyCode = 0;
    
    
    // Create a new keyboard event.
    public LemonEvent(int keyCode) {
        this.eventType = EVENT_TYPE_KEYBOARD_EVENT;
        this.keyCode = keyCode;
    }
    
    
    // Create a new mouse event.
    public LemonEvent(int mouseX, int mouseY, int mouseButton) {
        this.eventType = EVENT_TYPE_MOUSE_EVENT;
        this.mouseX = mouseX;
        this.mouseY = mouseY;
        this.mouseButton = mouseButton;
    }
}
