



## Part 2: Keyboard input and audio output and mouse input

For the visual part we will cover three different ways of interacting with the user. To get the necessary material, grab the updated version of `LemonWindow.java`, the new `LemonEvent.java`, and `tada.wav` from the `resources` directory of this repository and save it locally.

- switch refresh

### Big Picture: The `main()` method

while not done

    private static LemonWindow window;
    private static boolean stop = false;
    
    events = window.getInputEvents();
    
    handleEvent
    
    
    private static void handleEvent(LemonEvent event) {
        switch(event.eventType) {

        case LemonEvent.EVENT_TYPE_KEYBOARD_EVENT:
            handleKeyboardEvent(event);
            break;

        case LemonEvent.EVENT_TYPE_MOUSE_EVENT:
            handleMouseEvent(event);
            break;
        }
    }
    
    
Take a look at the `LemonEvent` class.

`EVENT_TYPE_*`
`MOUSE_*`
    
### Mouse input 1

`handleMouseEvent()`

        case LemonEvent.MOUSE_BUTTON_LEFT:
            window.setImage("black circle", event.mouseX, event.mouseY);
            break;
            
        case LemonEvent.MOUSE_BUTTON_RIGHT:
            window.setImage("white circle", event.mouseX, event.mouseY);
            break;
            
### Keyboard input

`handleKeyboardEvent()`

`KeyEvent.VK_ESCAPE`
stop

`KeyEvent.VK_R`
remove
`setImage(null, <x>, <y>)`

### Audio output

`KeyEvent.VK_SPACE`
`LemonWindow.playSound()`

bonus: find wav, `LemonWindow.playSound()` with 0 2nd arg

### Mouse input 2
            
        case LemonEvent.MOUSE_BUTTON_LEFT:
            window.setImage(BRUSHES[brushIndex], event.mouseX, event.mouseY);
            break;
            
        case LemonEvent.MOUSE_BUTTON_RIGHT:
            window.setImage(null, event.mouseX, event.mouseY);
            break;
            
    public static final String[] BRUSHES = {
            "white circle", "black circle", "slash", "backslash"
    };
    private static int brushIndex = 0;
            
            
        case LemonEvent.MOUSE_WHEEL_UP:
            brushIndex = (brushIndex - 1 + BRUSHES.length) % BRUSHES.length;
            System.out.println("Current brush: " + BRUSHES[brushIndex]);
            break;
            
        case LemonEvent.MOUSE_WHEEL_DOWN:
            brushIndex = (brushIndex + 1) % BRUSHES.length;
            System.out.println("Current brush: " + BRUSHES[brushIndex]);
            break;
        }
