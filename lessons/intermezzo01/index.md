
# Intermezzo 01

    Life is what happens while you are busy making other plans.
    
Not ashamed enough I'm pulling out old John Lennon to say: Anyone can outline the greatest plans to learn Java self-guided and at one's pace. But there will always be unexpected things happening that challenge those plans. Still working on some previous lesson, three weddings on one weekend, the hamster dies, during holidays the subconscious goes on general learning-strike, we've all been there. What do you do then? Work harder, take extra hours to make up for the time lost. Will we learn something? Eventually. Maybe. Is it fun? Well...

Or you just skip a week of learning the stuff you really want to learn. And then skip another week, because then you finally have the time to get back to it. And another, because the second hamster dies. And maybe another, because who likes things that last only for three weeks. Then continuity of regular practise is broken, which can be toxic to learning progress. 

This Intermezzo is intended to make up for it. Let's do something simple and fun that let's us keep our streak, but doesn't strain us.

## The Drama Generator

We will write a generator for brief stories, that could possibly make sense, but maybe won't. At least it should be entertaining. We start with some static text and make it more dynamic with each step. There will be three steps and an optional bonus step.

### Step 1: Constant drama

Let's write a short drama ourselves loosely inspired by the structure described in [Freytag's drama theory](https://en.wikipedia.org/wiki/Dramatic_structure#Freytag's_analysis). For now let's stick to five sentences (one for each part of the dramatic arc) featuring two characters.

1. Exposition: Mention both characters and a location.
2. Rise: Introduce a conflict.
3. Climax: Let one character do something crazy or outrageous.
4. Fall: Have the conflict resolved.
5. Catastrophe: Bring the story to an end. Maybe give an outlook.

Keep it at one sentence (or logical unit) per part. Don't use pronouns, but use full character names for now. Below is an example story. Ignore the sentence in brackets for now.

    One dark, dark night between here and there, Albert Einstein bumped into Batman.
    "Are you talking to me?", Batman said.
    (Later Albert Einstein arrived in the castle, where Batman already waited.)
    Albert Einstein slapped Batman with a fish.
    Then Batman blushed and looked at the ground with a smile in the face.
    "Cool beans?", Batman asked; "Cool beans.", Albert Einstein replied.

Write your story in a `Java` file. Save each sentence in a separate (static) class variable. Use one `System.out.println()` per sentence. This could look like this:
    
    public class DramaGenerator {
        
        public static final String EXPOSITION = "One dark, dark night...";
        // ...
        
        public static void main(String[] args) {
            System.out.println(EXPOSITION);
            // ...
        }
    }
         

### Step 2: Drama with random story

For each of the five parts above you wrote a sentence. First we will extend, then adapt the code slightly. 

Create a method with the following signature and return value. Let the method return a random element of the array. (This method could be reasonably written in one line.)

    public static String chooseRandomElement(String[] array)
    
For randomness, you will need an instance of the class `Random`. Create it as a class-variable with:

    private static Random random = new Random();

Now change the type of the variables for your class variables from `String` to `String[]`. Then bring in some variety: For each part, add two more sentences that match the criteria above to each array. (While this sounds like a for-loop, this will make things harder to read. Let's postpone loops and optimisation to when we are further down the road.)

Now let's bring everything together. In your `main()` method do the following: For each drama part, use the `chooseRandomElement()` method to select one element from the respective array and store it in a new variable. Then `println()` that variable like in the step before. (Technically that new variable is not necessary, but this will help in the next step.)

If you made it until here, you now should get some random stories already.

### Step 3: Drama with random story, characters, locations

You should now have five class variables of type `String[]`. Let's add two more: Add one `String[]` array for character names and one for location names. For both, make up at least five names. Maybe use something you have some attitude or feelings towards, like a graveyard or Donald Trump.

Since we have two characters, we need to pick two random values from the character array. However there is a chance we might pick the same name twice, which would break the story. We could just pick the second one and re-pick if it is equal to the first one, but this is a naive solution and we want to be smarter. In fact, this approach does not scale well: Imagine you wanted to pick 950 random elements from an array with 1000 elements. The naive approach will take a while here. A smarter solution must exist! And indeed it does. We will create the following method (note the `s`). 

    public static String[] chooseRandomElements(String[] array, int amount) 

Now you have two options: Either you try it yourself with some guidance (option 1) or use ready-made code from below (option 2). Make it dependent on your other workload this week. This Intermezzo should be fun and not tedious! The focus here is not on figuring out algorithms, but having silly fun with code. 

<details>
<summary><strong>Option 1:</strong> Click here to show the algorithm in pseudo-code.</summary>

_Note that if you chose to code the algorithm, this pseudo-code is written as you might find it in the wild. Use indices where necessary and mind that while humans prefer counting from 1, many programming languages do not. You might want to test that method with some static data, before you use it in the context of this exercise._

    take as parameters: input array, number of elements N 
    create a new "working" array of the same length as the input array
    create a new "output" array with length equal to N
    copy all elements from the input array to the working array
    for each i between 1 and N:
        choose a random element from the working array, but not from the last i elements
        put the chosen element into the output array
        replace the chosen element with the i-th element from the back in the working array
    return the output array

</details>

<details>
<summary><strong>Option 2:</strong> Click here to show copy-paste-ready Java code.</summary>

    /*
     * Select `amount` random elements from `array` and put it into a new 
     * array of length `amount`.
     */
    public static String[] chooseRandomElements(String[] array, int amount) {
        String[] arrayCopy = new String[array.length];
        String[] selectedElements = new String[amount];
        int randomIndex;
        
        // Copy original array elements into working array.
        for(int i = 0; i < arrayCopy.length; i++) {
            arrayCopy[i] = array[i];
        }
        
        // From working array copy only the first required elements. Replace 
        // elements to avoid picking the same next time.  In each iteration, 
        // only choose from the first (array length - i) elements.
        for(int i = 0; i < selectedElements.length; i++) {
            randomIndex = random.nextInt(arrayCopy.length - i);
            selectedElements[i] = arrayCopy[randomIndex];
            arrayCopy[randomIndex] = arrayCopy[arrayCopy.length - 1 - i];
        }
        
        return selectedElements;
    }

</details>

Now here comes the magic: Use the method [`String.format()`](https://docs.oracle.com/en/java/javase/14/docs/api/java.base/java/lang/String.html#format(java.lang.String,java.lang.Object...)) with the sentences that now have place holders. The first argument to that method is a so-called format string. All subsequent arguments to that method are used to fill placeholders, if the format string contains any. (Yes, that method takes a variable number of arguments.) Check the example below and note the order of arguments here and in what order they are printed.

    String whom = "my ducks";
    String how = "joy";
    String what = "my enemies";
    String text = String.format("I like to feed %3$s to %1$s with %2$s.", whom, how, what);
    System.out.println(text);
    // output: I like to feed my enemies to my ducks with joy.

So now we need to put placeholders into the existing sentences instead of hard-coded names. In other words, we turn our static sentences into format strings. Use `%1$s` for the first character, `%2$s` for the second character and `%3$s` for the location where applicable. You might want to use the search-and-replace function of your favourite text editor.

(There is actually a lot more to say about format strings, as they are very powerful, flexible and have a long tradition. Also, the placeholders mentioned above are a bit Java-specific. Covering all of that here is beyond the intended scope, but should be discussed at some other point in time.)

Now let's bring everything together again: Use `chooseRandomElements()` to select the right number of character and location names you need. Use `String.format()` to populate our randomly-selected format strings (previously known as sentences) with randomly selected character and location names.

And then: Go boast! You created a lot of random silliness. Go and share your creation. :-)

### Bonus step: Random, random, random where I look

Now we got something that can already create some nifty stories. If you would have three format strings (sentences) per drama part to choose from, six possible locations and five character names, you could already generate `3^5 * 6 * 5*4 = 29160` different stories! Of course they will be similar or repetetive to some degree, but this is to show that with not too much code you can already produce a great variety of possible output.

This last bonus step is challenging you to come up with an own extension of your current drama generator. For instance, in the example provided in Step 1 was taken straight out of one working drama generator. You will notice the sentence in brackets, which adds an additional change of location to our drama structure.

Many other extensions are possible! You could add adjectives in front of character names (and even randomise the number of them). You could add a list of weapons to be used or a list of reasons to get into a fight. The length of the drama could be varied by using `chooseRandomElement()` for the format strings a random number of times. Pick one simple idea and try to implement it yourself.
